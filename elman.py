from EAlib import *
from data import generate_mk
from data import generate_takens_training_data
from utility import *

class elman(individual):
    def __init__(self, output_activation_type,n2,sigmoid_slopes):
        individual.__init__(self,1) #1 objective
        '''
        Create network with specified shape
        Parameters:
        -----------
        n1 = size of input layers (# of inputs) 
        n2 = size of hidden layer
        n3 = size of ouput layer 
        for this case, n1 = n3 = 1 because we are predicted one d time series 
        
        w12 = numpy array of weights between input and hidden layer
        w23 = numpy array of weights between hidden layer and output 
        
        wMin = min weight value
        wMax = max weight value
        '''

        self.sigmoid_slopes = sigmoid_slopes
        self.output_activation_type = output_activation_type
        
        n1 = 1
        n3 = 1

        self.state = np.zeros((n2,1))
        self.output = np.zeros((n3,1))
        
        self.n1 = n1
        self.n2 = n2
        self.n3 = n3
               
        #FP initialization
        #Numpy is (row,column)        
        #In general, for NN layers, it is #destinationNodesX#sourceNodes
        w12 = np.random.uniform(-1,1,(n2,n1)) #For input to hidden, we want a #hiddenX#input
        wb12 = np.random.uniform(-1,1,(n2,1))
        w23 = np.random.uniform(-1,1,(n3,n2)) #For hidden to output, we want a #outputX#hidden
        wb23 = np.random.uniform(-1,1,(n3,1))
        wc2 = np.random.uniform(-1,1,(n2,n2))
        wbc2 = np.random.uniform(-1,1,(n2,1))
        self.cms = [w12,w23,wc2]
        self.cmbs = [wb12,wb23,wbc2]

        ##Ternary initalization from FP initialization
        for cm in self.cms:
            for index, entry in np.ndenumerate(cm):
                if cm[index] < (-2/float(3)):
                    cm[index] = -1
                elif cm[index] > (2/float(3)):
                    cm[index] = 1
                else:
                    cm[index] = 0
        
        #same thing for biases
        for cmb in self.cmbs:
            for index, value_index in np.ndenumerate(cmb):
                if cmb[index] < (-2/float(3)):
                    cmb[index] = -1
                elif cmb[index] > (2/float(3)):
                    cmb[index] = 1
                else:
                    cmb[index] = 0
        
        
        
        self.predicted_outputs = list()

        self.total_num_entries = n1*n2+n2+n3*n2+n3+n2*n2+n2
        self.cutoff_points = [n1*n2,n1*n2+n2,n1*n2+n2+n3*n2,n1*n2+n2+n3*n2+n3,n1*n2+n2+n3*n2+n3+n2*n2]

                        
    #a2 and a3 are the activations of the hidden and output layer respectively
    def predict(self,input):

        a2 = np.dot(self.cms[0],input) + np.dot(self.cms[2],self.state) + self.cmbs[0] + self.cmbs[2]

        o2 = sigmoid(a2*self.sigmoid_slopes[0])
        self.state = o2
        
        a3 = np.dot(self.cms[1],o2) + self.cmbs[1]
        if self.output_activation_type == 0:
            o3 = sigmoid(a3*self.sigmoid_slopes[1])
        else:
            o3 = np.tanh(a3*self.sigmoid_slopes[1])
        self.output = o3
        
        return o3

##For now I'm initializing EA parameters when calling run(), but the more domain specific parameters during initialization
class ElmanEA(EA):
    def __init__(self,hidden_layer_size,sigmoid_slopes,dataset_config,file_name):
        EA.__init__(self)
        self.hidden_layer_size = hidden_layer_size
        self.sigmoid_slopes = sigmoid_slopes
        self.num_training_samples = num_training_samples
        self.dataset = dataset_config[0]
        self.output_activation_type = dataset_config[1]
        self.dimension_size = dataset_config[2]
        self.tau = dataset_config[3]
        self.file_name = file_name

        self.test_curve = list()
        [self.X_train,self.y_train,self.X_test,self.y_test] = generate_takens_training_data(self.dataset,self.tau,self.dimension_size)
        self.X_data = []
        self.y_data = []

    def create_individual(self):
        new_individual = elman(self.output_activation_type,self.hidden_layer_size,self.sigmoid_slopes)
        return new_individual

    def configure_population(self):
        self.X_data = self.X_train

    def generate_semantics(self,current_individual):
        #print current_individual.id
        current_individual.predicted_outputs = list()                   
        for training_sample in self.X_data:
            current_individual.state = 0 #make sure the state is cleared before each evaluation ###!!!!NK it was self.state before
            for input in training_sample:
                current_individual.predict(input) #by default input is a scalar, we want it to be a 1x1 array
            current_individual.predicted_outputs.append(current_individual.output[0][0])
        return current_individual      

    def evaluate_fitness(self,current_individual):
        current_individual.fitnesses[0] = 1/float(calculate_RMSE(self.y_train,current_individual.predicted_outputs)) #we want to maximize fitness 
        current_individual.predicted_outputs = list() #NK
        
                   
    def mutate_parent(self,current_individual):
        rand_entry_index = np.random.random_integers(1,current_individual.total_num_entries)
        if (rand_entry_index <= current_individual.cutoff_points[0]):
            tg_cm = current_individual.cms[0]
        elif (rand_entry_index <= current_individual.cutoff_points[1]):
            tg_cm = current_individual.cmbs[0]
        elif (rand_entry_index <= current_individual.cutoff_points[2]):
            tg_cm = current_individual.cms[1]
        elif (rand_entry_index <= current_individual.cutoff_points[3]):
            tg_cm = current_individual.cmbs[1]
        elif (rand_entry_index <= current_individual.cutoff_points[4]):
            tg_cm = current_individual.cms[2]
        else:
            tg_cm = current_individual.cmbs[2]  
            
        tg_im_dimensions = np.shape(tg_cm)
        tg_row_id = np.random.random_integers(0,tg_im_dimensions[0]-1)
        tg_column_id = np.random.random_integers(0,tg_im_dimensions[1]-1)
        inc_or_dec = np.random.random_integers(0,1) #0 is decrement, 1 is increment

        if (inc_or_dec == 0):
            if tg_cm[tg_row_id,tg_column_id] == 0:
                tg_cm[tg_row_id,tg_column_id] = 1
            elif tg_cm[tg_row_id,tg_column_id] == -1:
                tg_cm[tg_row_id,tg_column_id] = 0
            else:
                tg_cm[tg_row_id,tg_column_id] = -1

        else:
            if tg_cm[tg_row_id,tg_column_id] == 0:
                tg_cm[tg_row_id,tg_column_id] = -1
            elif tg_cm[tg_row_id,tg_column_id] == -1:
                tg_cm[tg_row_id,tg_column_id] = 1
            else:
                tg_cm[tg_row_id,tg_column_id] = 0

  
    def crossover_parents(self,parent1,parent2):
        for tg_im_index in range(3):
            for w_or_b in range(2):
                if (w_or_b == 0):
                    tg_m1 = parent1.cms[tg_im_index]
                    tg_m2 = parent2.cms[tg_im_index]
                else:
                    tg_m1 = parent1.cmbs[tg_im_index]
                    tg_m2 = parent2.cmbs[tg_im_index]

                tg_im_dimensions = np.shape(tg_m1)
                for row_id in range(tg_im_dimensions[0]):
                    for col_id in range(tg_im_dimensions[1]):
                        sample = np.random.random_sample()
                        if (sample > 0.5):
                            tg_m1[row_id][col_id] = copy.deepcopy(tg_m2[row_id][col_id])
                        else:
                            tg_m2[row_id][col_id] = copy.deepcopy(tg_m1[row_id][col_id]) 

    def stream_out_other_curves(self,current_generation):
        #test fittest current_individual on out of sample data
        sorted_individuals = copy.deepcopy(self.population)
        sorted_individuals.sort(key=lambda x: x.fitnesses[0], reverse=True)
        fittest_individual = sorted_individuals[0]
        self.X_data = self.X_test
        self.generate_semantics(fittest_individual)
        test_fitness = 1/float(calculate_RMSE(self.y_test,fittest_individual.predicted_outputs))
        print("Test fitness is " + str(test_fitness))
        self.test_curve.append(test_fitness)   
            
        #save results
        if ((current_generation+1)%self.save_frequency == 0 or current_generation == self.num_gen-1): #always save on the last generation
            if (self.file_name != ""):
                self.save_lists([self.test_curve],self.file_name + "test_curve")
                self.save_lists([self.y_test,fittest_individual.predicted_outputs],self.file_name + "time_series")





#######

num_training_samples = 500
mk_series = generate_mk()[0:num_training_samples*2]
mk_series_config = [mk_series,0,3,2]
series_config = mk_series_config  
population_size = 1000
pressure_trial = 10
hidden_trial = 9
max_num_evals = 200000
filename = "hope122"
sigmoid_trial = [3,3]
mutation_prob = 1
currentTrial = ElmanEA(hidden_trial,sigmoid_trial,series_config,filename)
if __name__ == '__main__':   
    currentTrial.run(population_size,5,10,mutation_prob,0,2) #mutation rate is set to 0 because it is not used
    #currentTrial.debug(4)