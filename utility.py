import numpy as np
import math
import copy
# TO DO -> make sure the result is floating point 
def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def calculate_RMSE(theoretical, experimental):
    total_error_sq = 0
    for index,entry in enumerate(theoretical):
        sample_error_sq = (experimental[index] - theoretical[index])**2
        total_error_sq += sample_error_sq
    RMSE = math.sqrt(total_error_sq/float(len(experimental)))

    return RMSE

def flatten(l):
    flat_list = list()
    for sublist in l:
        for item in sublist:
            flat_list.append(item)
    return flat_list

#Takes a list of lists, where the lower level list represents the values for the multiple objectives
def non_dominated_sort(dataset):
    #print dataset
    numAttributes = len(dataset[0])
    list_size = len(dataset)
    num_ranked_individuals = 0
    dominated_counts = list()
    domination_sets = list()
    fronts = list()
    front0 = list()
    for i in range(list_size):
        domination_set = list()
        dominated_count = 0
        for j in range(list_size):
            numBetterAttributes_i = 0
            numBetterAttributes_j = 0
            numEqualAttributes = 0
            for attributeId in range(numAttributes):
                if dataset[i][attributeId] > dataset[j][attributeId]:
                    numBetterAttributes_i += 1
                elif dataset[i][attributeId] < dataset[j][attributeId]:
                    numBetterAttributes_j += 1
                else:
                    numEqualAttributes += 1

            
            if numBetterAttributes_j == 0 and numEqualAttributes != numAttributes: #if j is never better than i and i is better than j at at least 1 fitness
                 domination_set.append(j)
            elif numBetterAttributes_i == 0 and numEqualAttributes != numAttributes: #j dominates i
                 dominated_count += 1
            

            '''
            if numBetterAttributes_i == numAttributes: #i dominates j
                domination_set.append(j)
            elif numBetterAttributes_j == numAttributes: #j dominates i
                dominated_count += 1
            '''
            
        if dominated_count == 0:
            front0.append(i)
            num_ranked_individuals += 1
        domination_sets.append(domination_set)
        dominated_counts.append(dominated_count)

    fronts.append(front0)
    front_counter = 0
    while(num_ranked_individuals != list_size):
        new_front = list()
        for i in fronts[front_counter]:
            for j in domination_sets[i]:
                dominated_counts[j] -= 1
                if dominated_counts[j] == 0:
                    new_front.append(j)
                    num_ranked_individuals +=1                        
        fronts.append(new_front)
        front_counter +=1

    return fronts

def NonDominatedMax(dataset):
    numAttributes   = len(dataset[0])
    list_size       = len(dataset)
    curBestFront    = list()
    curBestFrontIdx = list()
    for i in range(list_size):
        frontSize = len(curBestFront)
        if frontSize == 0:
            curBestFront.append(dataset[i])
            curBestFrontIdx.append(i)
        else:
            for j in range(frontSize):
                numBetterAttributes_i = 0
                numBetterAttributes_j = 0
                numEqualAttributes = 0
                flag                 = 0
                for attributeId in range(numAttributes):
                    if   dataset[i][attributeId] > curBestFront[j][attributeId]:
                        numBetterAttributes_i += 1
                    elif dataset[i][attributeId] < curBestFront[j][attributeId]:
                        numBetterAttributes_j += 1
                    else:
                        numEqualAttributes += 1    
            
                if numBetterAttributes_j == 0 and numEqualAttributes != numAttributes: #If i dominates j
                     curBestFront.pop(j)
                     curBestFront.append(dataset[i])
                     curBestFrontIdx.pop(j)
                     curBestFrontIdx.append(i)
                elif numBetterAttributes_i == 0 and numEqualAttributes != numAttributes: #j dominates i
                    pass
                else: #They are both part of the fittest front
                    flag = 1
            if flag == 1:
                curBestFront.append(dataset[i])
                curBestFrontIdx.append(i)

    return curBestFrontIdx
            

def combinations (choices):
    queue = [choices]
    results = list()

    while (len(queue)>0):
        currentChoices = queue.pop()
        if currentChoices not in results:
            results.append(currentChoices)
        numChoices = len(currentChoices)

        for i in range(numChoices):
            temp = copy.deepcopy(currentChoices)
            temp.pop(i)
            if len(temp) != 0:
                queue.append(temp)

    return results

#def SplitList(fullList,numSublists):
def SplitList(fullList,sublistSize):
    #sublistSize = len(fullList)/numSublists
    return [fullList[i:i + sublistSize] for i in xrange(0, len(fullList), sublistSize)]