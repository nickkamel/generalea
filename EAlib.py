###TO DO -> list of pointers to different fitness functions for multi objective optimization
###TO DO -> add an option to save the genotype of every individual or just the best individual of every generation
###TO DO -> (Eventually) save tables and graphs to a latex PDF

import numpy as np
np.set_printoptions(precision=4)
import math
import copy
#import matplotlib.pyplot as plt
from utility import flatten, non_dominated_sort, NonDominatedMax, SplitList
from multiprocessing import Pool
import sys
#sys.path.insert(0, 'C:\Users\Nick\Documents\Visual Studio 2015\Projects\EAserver')
sys.path.insert(0, 'C:\Users\nickk\Documents\Visual Studio 2015\Projects\EAserver')
from flk import InsertGenericIndividuals, RetrieveTrialId, ClearArchive
   
class individual:
    def __init__(self):
        self.fitnesses = list()
        self.id = 0
        self.idInArchive = 0

class EA:
    def __init__(self):
        self.population = list()
        self.archive = list()
        self.distanceMatrixArchive = None
        self.parents = list()
        self.offspring = list()
        self.cumulative_distributions = list()
        self.idCounter = 0 #The 1st individual with have id = 1 so that it is consistent with flk SQL primary key that starts indexing at 1          
               
    def initialize_population(self):
        for i in range(self.num_ind):
            new_individual = self.create_individual()
            self.idCounter += 1 
            new_individual.id = self.idCounter 
            self.offspring.append(new_individual)

    def configure_population(self):
        pass

    def generate_semantics_offspring(self):
        self.configure_population()
        if self.multiprocessing_option == 0:   #no multiprocessing     
            for current_individual in self.offspring: #we only evaluate the offspring because the parents have already been evaluated
                self.generate_semantics(current_individual)
        elif self.multiprocessing_option == 1:  #multiprocessing but no chunks 
            pool = Pool(4) 
            pool_outputs = pool.map(self, self.offspring)
            for id,output in enumerate(pool_outputs): 
                self.offspring[id] = output
            pool.close()
            pool.join()
        elif self.multiprocessing_option == 2:  #multiprocessing with chunks 
            numChunks = 4 #If number of chunks == num processes, then if a chunk finishes early, there is a free core
            #But if num chunks > num processes there is more overhead time
            n = len(self.offspring)/numChunks
            offspring_chunks = [self.offspring[i:i + n] for i in xrange(0, len(self.offspring), n)]
            pool = Pool(4)  
            pool_outputs = pool.map(self, offspring_chunks)

            for id,output in enumerate(pool_outputs): 
                self.offspring[id*n:id*n+n] = output
            pool.close()
            pool.join()
        else: #we are implementing the multiprocessing at the specific EA level
            pass


    def generate_semantics_chunks(self,offspring_chunk):
        offspring_chunk_updated = list()
        for hope,offspring in enumerate(offspring_chunk):
            print hope
            offspring_chunk_updated.append(self.generate_semantics(offspring))
        return offspring_chunk_updated

    def __call__(self,x):
        if self.multiprocessing_option == 1:
            return self.generate_semantics(x)
        elif self.multiprocessing_option == 2:
            return self.generate_semantics_chunks(x)

    def evaluate_fitness_offspring(self):
        #Calculate all potential fitnesses
        for current_individual in self.offspring:
            current_individual.fitnesses = list()
            current_individual.availableFitnesses = list()
            self.evaluate_fitness(current_individual)
        if 'novelty' in self.selectedFitnesses:
            if self.survivor_selection_method == 1: #age based
                self.calculate_novelty(self.offspring)
            elif self.survivor_selection_method == 0 or self.survivor_selection_method == 2: #GENITOR or TS. Update the parents' novelty and local competition scores
                self.calculate_novelty(self.population  + self.offspring)

        #Select fitnesses
        for pop in [self.offspring,self.archive]:       
            for ind in pop:
                for selectedFitness in self.selectedFitnesses:
                    ind.fitnesses.append(ind.potentialFitnesses[selectedFitness])
                for displayedFitness in self.availableFitnesses:
                    ind.availableFitnesses.append(ind.potentialFitnesses[displayedFitness])

    def calculate_novelty(self,input):
        maxArchiveSize = self.fitnessParams[0]
        k = self.fitnessParams[1]
        batch = list()

        #Form distance matrix
        inputPlusArchive = self.archive + input
        curArchiveSize = len(self.archive)
        distanceMatrix = np.zeros((len(inputPlusArchive),len(inputPlusArchive)))
        distanceMatrix[0:curArchiveSize,0:curArchiveSize] = self.distanceMatrixArchive #!!!Might need a deepcopy here #This is the first quadrant
        numRows = distanceMatrix.shape[0]
        numCols = distanceMatrix.shape[1]
        for i in range(self.num_ind): #With this i we are building the rows of the bottom two quadrants
            for j in range(len(self.archive)+i): # j in range(archive size) builds  the bottom left quadrant in its entirety. The + i part builds the lower triangular half of the bottom right.
                batch.append((inputPlusArchive[i], inputPlusArchive[j]))

        batchResults = self.calculatePhenotypeDistanceBatch(batch)

        batchIdx = 0
        for i in range(self.num_ind): #With this i we are building the rows of the bottom two quadrants
            for j in range(len(self.archive)+i): # j in range(archive size) builds  the bottom left quadrant in its entirety. The + i part builds the lower triangular half of the bottom right.
                curDistance = batchResults[batchIdx]
                distanceMatrix[curArchiveSize + i][j] = curDistance
                distanceMatrix[j][curArchiveSize + i] = curDistance
                batchIdx += 1 


        #print "Distance Matrix"
        #print distanceMatrix

        #Sort distance matrix
        sortedDistanceValues = np.sort(distanceMatrix, axis = 1) #sort each row
        sortedDistanceKeys = np.argsort(distanceMatrix, axis = 1)

        #print "Sorted values"
        #print sortedDistanceValues
        #print "Sorted keys"
        #print sortedDistanceKeys

        #Calculate sparseness
        sparsenessScores = np.zeros((numRows))
        for i in range(numRows):
            sparsenessScores[i] = sum(sortedDistanceValues[i][0:k])
        #print "Sparseness Scores"
        #print sparsenessScores

        ####LOCAL COMPETITION
        ###!!!!! The archive members don't have their local competition scores updated so they are erroneous. 
        neighborsInds = list()
        for i in range(self.num_ind):
            #neighborsInd = [i] #We also include itself as a neighbor
            neighborsInd = list()
            #compare individual against k neighbors that aren't in the archive
            for j in range(len(inputPlusArchive)):
                neighborId = sortedDistanceKeys[curArchiveSize + i][j]
                if neighborId >= curArchiveSize: #!! Don't compete against neighbors that are in the archive
                    neighborsInd.append(neighborId-curArchiveSize)
                    if len(neighborsInd) > k:
                        break
            neighborsInds.append(neighborsInd)
            for selectedFitness in self.selectedFitnesses: #this block computes the local ranking
                if "Local" in selectedFitness:
                    globalFitness = selectedFitness.replace("Local","")
                    ut = [input[n].potentialFitnesses[globalFitness] for n in neighborsInd]
                    #print ut
                    newlist = sorted(range(len(ut)), key=lambda x: ut[x], reverse=False) #Since higher fitness is better, we want the best performing solutions to be ranked at the bottom
                    #print newlist
                    input[i].potentialFitnesses[selectedFitness] = newlist.index(0) #return the ranking of the individual
        #print "neighborsInds"
        #print neighborsInds
        ####

        #Moved this before storing archive. Otherwise, the archive members were missing novelty score.
        for indId in range(self.num_ind): 
            input[indId].potentialFitnesses['novelty'] = sparsenessScores[indId]/float(k) 

        #Reduce archive size
        #Only run this block of code if we want an archive.
        if maxArchiveSize !=0:
            while (curArchiveSize > maxArchiveSize):
                #Sort sparseness (by ascending)
                sortedSparsenessScoresValues = np.sort(sparsenessScores)
                sortedSparsenessScoresKeys   = np.argsort(sparsenessScores)
                #print "Sorted Sparseness Values and Keys"
                #print sortedSparsenessScoresValues
                #print sortedSparsenessScoresKeys

                #Remove least sparse entry
                entryToRemove = sortedSparsenessScoresKeys[0]
                #print "Entry To Remove is " + str(entryToRemove)
                distanceMatrix = np.delete(distanceMatrix,entryToRemove,0) #remove row
                distanceMatrix = np.delete(distanceMatrix,entryToRemove,1) #remove column
                #print "Updated distance matrix"
                #print distanceMatrix
                numRows -= 1
                numCols -= 1
                sortedDistanceValues = np.delete(sortedDistanceValues,entryToRemove,0) #remove row only
                sortedDistanceKeys   = np.delete(sortedDistanceKeys,entryToRemove,0) #remove row only
                #print "Sorted Distance Keys after removal"
                #print sortedDistanceKeys
                sparsenessScores = np.delete(sparsenessScores,entryToRemove)
                sortedSparsenessScoresValues = np.delete(sortedSparsenessScoresValues,entryToRemove)
                #print "Sorted Sparseness Values after Removal"
                #print sortedSparsenessScoresValues
                inputPlusArchive.pop(entryToRemove)
                curArchiveSize -= 1


                #Update sparseness scores
                for i in range(numRows):
                    rankingOfDeletedEntry = np.nonzero(sortedDistanceKeys[i] == entryToRemove)[0][0]
                    if rankingOfDeletedEntry <= k-1: #This is so I don't have to re-summate the entire row. I can just take into account the changed element.
                        sparsenessScores[i] -= sortedDistanceValues[i][rankingOfDeletedEntry]
                        sparsenessScores[i] += sortedDistanceValues[i][k+1]

                #print "Sparseness values after recalculation"
                #print sparsenessScores

            self.archive = copy.deepcopy(inputPlusArchive)
            #Give each archive member a unique ID, even if an individual remains in the archive from one generation to the next (because its novelty score will have changed)
            for i in range(len(self.archive)):
                self.idCounter += 1
                self.archive[i].id = self.idCounter
            self.distanceMatrixArchive = copy.deepcopy(distanceMatrix)  


    def select_parents(self): #Not sure if the deep copies here are overkill
        if (self.parent_selection_method == 0):
            self.parents = copy.deepcopy(self.tournament_selection(self.population))
        elif (self.parent_selection_method == 1):
            self.parents = copy.deepcopy(self.ranked_selection(self.population))
        elif (self.parent_selection_method == 2):
            self.parents = copy.deepcopy(self.fitness_proportional_selection(self.population))
        elif (self.parent_selection_method == 3):
            self.parents = copy.deepcopy(self.FittestFrontParents(self.population))
        elif (self.parent_selection_method == 4):
            self.parents = copy.deepcopy(self.offspring) #Uniform parent selection

    def build_front_cumulative_distributions(self):
        s = 1.95
        self.cumulative_distributions.append([1]) #Handle the case where there is only one front
        for num_fronts in range(2,self.num_ind+1): #build distribution for all possible number of fronts
            ranked_scores = [((2-s)/float(num_fronts) + 2*i*(s-1)/(num_fronts*(num_fronts-1)))**1 for i in range(num_fronts)] #This equation doesn't work if there is only one front
            sum_ranked_scores = sum(ranked_scores)
            ranked_scores_normalized = [ranked_score/float(sum_ranked_scores) for ranked_score in ranked_scores] ##Based on EA book they should already be normalized??
            #cumulative_distribution = [sum(ranked_scores_normalized[num_fronts-i:]) for i in range(1,num_fronts+1)]   ###SLOW         
            cumulative_distribution = list()
            cumulative_value = 0
            for i in range(1,num_fronts+1):
                cumulative_value = cumulative_value + ranked_scores_normalized[num_fronts-i]
                cumulative_distribution.append(cumulative_value)
            self.cumulative_distributions.append(cumulative_distribution)      


    #For single objective, the number of fronts is equal to the population size
    def ranked_selection(self,pool):
        selection = list()
        #rank the individuals into fronts
        fronts = non_dominated_sort([p.fitnesses for p in pool])
        print fronts
        num_fronts = len(fronts)
        print "Number of fronts " + str(num_fronts)
      
        cumulative_distribution = self.cumulative_distributions[num_fronts-1] #The 0th entry is the cumulative distributions for 1 front

        num_sampled_individuals = 0
        for ind_id in range(len(pool)):
            sample = np.random.random_sample()
            for sampled_front_id,cutoff in enumerate(cumulative_distribution):
                if (sample < cutoff):
                    break

            #sample an individual from the chosen front
            front_members_id = fronts[sampled_front_id]
            sample_front_member_id = np.random.randint(0,len(front_members_id))
            selection.append(copy.deepcopy(pool[front_members_id[sample_front_member_id]])) #WITHOUT DEEPCOPY, if an individual is selected multiple times as a parent...
            #...Then we will have multiple individuals referring to the same instance 

        return selection
    
    def FitnessProportionalSelection():
     selectedInds = list()
     scaledFitnesses = self.ScaleFitnesses()
     #Build roulette wheel
     rwCutoffs = [sum(scaledFitnesses[0:i+1]) for i in range(self.num_ind)]
     rwCutoffs = rwCutoffs/float(rwCutoffs[:-1]) #normalize it so that the last cutoff has a value of 1
     for i in range(partitionSize*self.num_ind):
          selectedIndId = SampleDistribution(rwCutoffs)
          selectedInd = copy.deepcopy(self.population[selectedIndId])
          selectedInds.append(selectedInd)
     return selectedInds

    def SampleDistribution(cutoffs):
         for id,cutoff in enumerate(cutoffs):
              if sample <= cutoff:
                   return id

    def ScaleFitnesses():
         scaledFitnesses = list()
         numFitnesses = len(population[0].fitnesses)
         for individual in self.population:
              scaledFitness = 1
              for fitnessId in range(numFitnesses):
                   scaledFitness *= self.metricMappings[fitnessId][individual.fitnesses[fitnessId]]
              scaledFitnesses.append(scaledFitness)
         return scaledFitnesses
    
    def BuildMetricMappings(scalingParams):
        metricMappings = dict()

        for fitnessId,scalingParam in enumerate(scalingParams):
            start = scalingParam[0]
            stop = scalingParam[1]
            fitnessDelta = scalingParam[2]
            if start>stop:
                incOrDec = -1
            else:
                incOrDec = 1

            currentScaledValue = 1/float(fitnessDelta)
            currentMetricMapping = dict()
            for metricValue in range(start,stop+incOrDec,incOrDec):
                currentScaledValue = currentScaledValue * fitnessDelta
                currentMetricMapping[metricValue] = currentScaledValue

            metricMappings[fitnessId] = currentMetricMapping

        return metricMappings

    def tournament_selection(self,pool):
        winners = list()
        for i in range(self.num_ind): 
            tournament_winner = self.perform_tournament_round(pool)
            winners.append(copy.deepcopy(tournament_winner)) #!!! Added deepcopy
        return winners

    def perform_tournament_round(self,pool):
        participants = list()
        for i in range(self.num_tournament_participants):
            current_participant_id = np.random.randint(0,self.num_ind)
            current_participant = pool[current_participant_id] #This is with replacement, which reduces selection pressure e.g. if I have inds, the strongest one won't quite saturate immediately
            participants.append(current_participant)

        fittestFront = non_dominated_sort([p.fitnesses for p in participants])[0]
        return participants[fittestFront[0]] #arbitrarily take the first element of best front

    def FittestFrontParents(self,pool):
        fittestFront = NonDominatedMax([p.fitnesses for p in pool])
        parents = list()
        for i in range(self.num_ind): 
            sample = np.random.randint(0,self.num_ind)
            parents.append(copy.deepcopy(pool[sample]))
        return parents


    def mutate_parent(self,parent):
        pass
        
    def crossover_parents(self,parent1,parent2):
        pass

    def produce_offspring(self):
        self.offspring = copy.deepcopy(self.parents) #This deepcopy prevents children from having same memory location as parents
        ###
        for off in self.offspring:
            off.fitnesses[0] = 0 ###! This is a glitch for multi objective
        ###
        num_parents_pairs = self.num_ind/2
        for i in range(num_parents_pairs):
            sample = np.random.random_sample()
            parent1 = self.offspring[2*i]
            parent2 = self.offspring[2*i+1]
            #if (parent1 == parent2):
            #    print "!!!!ADD DEECOPY somewhere!!!!!!!"
            self.idCounter += 1
            parent1.id = self.idCounter
            self.idCounter += 1
            parent2.id = self.idCounter
            
            if (sample < self.mutation_prob):
                self.mutate_parent(parent1)
                self.mutate_parent(parent2)
            else:
                self.crossover_parents(parent1,parent2)

        ###!!! We'll soon find out whether this was necessary
        '''
        ###RENAME offspring
        id = 1+self.curGen*self.num_ind
        for i in range(len(self.offspring)):
            self.offspring[i].id = id
            id +=1
        '''
  
    def select_survivors(self):
        population_plus_offspring = self.population  + self.offspring #after I get the EA working, I need to see how many deep copies I can remove
        if self.survivor_selection_method == 0: ##GENITOR: replace worst
            population_plus_offspring_fronts_ids = flatten(non_dominated_sort([p.fitnesses for p in population_plus_offspring]))[0:self.num_ind]
            self.population = [population_plus_offspring[id] for id in population_plus_offspring_fronts_ids]

        elif self.survivor_selection_method == 1: #Age based
            self.population = self.offspring #I don't think I need deep copy here


        elif self.survivor_selection_method == 2: ##Tournament selection
            self.population = self.tournament_selection(population_plus_offspring) 

        '''
        ###RENAME population
        id = 1+self.curGen*self.num_ind
        for i in range(len(self.population)):
            self.population[i].id = id
            id +=1
        '''

    def InsertIndividuals(self,popType):
        if popType == 0:
            individualsToInsert = self.offspring
        elif popType == 1:
            individualsToInsert = self.population
        elif popType == 2:
            individualsToInsert = self.archive

        '''
        indsToInsertChunks = SplitList(individualsToInsert,1000)
        for chunk in indsToInsertChunks: #Indent the rest of code in the function. !!! But this won't work for InsertGenoPheno
        '''
        rows = list()
        for ind in individualsToInsert:
            row = (ind.id,self.trialId,ind.availableFitnesses,popType,self.curGen)
            rows.append(row)

        InsertGenericIndividuals(rows)
        self.InsertGenoPheno(popType)

    def print_fitnesses(self,pool):
        for obj_id in range(len(self.availableFitnesses)):
            print "Objective " + str(obj_id)
            print [round(ind.availableFitnesses[obj_id],2) for ind in pool]

   #crossover prob = 1-mutation_prob
    def run(self,num_ind,num_gen,selection_method, selection_params,survivor_selection_method,mutation_prob,mutation_rate,selectedFitnesses,availableFitnesses,fitnessParams,multiprocessing_option):
        self.boolStore = 1 #!!!!

        if selectedFitnesses == ['placeholder']: #!!! super quick and dirty
            murderInitialPop = 0
        else:
            murderInitialPop = 1
        murderInitialPop = 0 #override the above

        cleanFraction = 10
        cleanInitialPopSize = num_ind/cleanFraction

        self.selectedFitnesses = selectedFitnesses
        self.availableFitnesses = availableFitnesses
        self.fitnessParams = fitnessParams
        self.num_ind = num_ind
        self.num_gen = num_gen
        self.parent_selection_method = selection_method
        if self.parent_selection_method == 0:
            self.num_tournament_participants = int(selection_params[0]) #in this case selection_params is a list of a single number
        elif self.parent_selection_method == 2:
            self.metricMappings = self.BuildMetricMappings(selection_params)
        self.mutation_prob = mutation_prob

        self.survivor_selection_method = survivor_selection_method #0 is genitor
        self.mutation_rate = mutation_rate
        self.multiprocessing_option = multiprocessing_option  
        self.build_front_cumulative_distributions() #Building the front distributions requires knowledge of the population size

        self.trialId = RetrieveTrialId()
        self.curGen = 0
        print ("Generation " + str(self.curGen))
        if murderInitialPop == 1:
            self.population = cleanFraction*self.runRandom(cleanInitialPopSize)
        else:
            print "Initializing population"
            self.initialize_population() #Treat the initial population like it is an offspring population. Initialize population creates an intial offspring but leaves population blank
            print "Generating semantics"
            self.generate_semantics_offspring()
            print "Evaluating fitnesses" 
            self.evaluate_fitness_offspring()
            #self.InsertIndividuals(0)
            print "Selecting survivors"
            self.select_survivors() #In this case, they are all survivors
        self.print_fitnesses(self.population)
        if self.boolStore == 1:
            self.InsertIndividuals(1)
            if 'novelty' in self.selectedFitnesses and self.fitnessParams[0]>0:
                self.InsertIndividuals(2)
        #print self.population
        for current_generation in range(1,self.num_gen):
            print ("Generation " + str(current_generation))
            self.curGen = current_generation
            print "Selecting parents"
            self.select_parents()
            print "Producing offspring"
            self.produce_offspring()
            print "Generating semantics"
            self.generate_semantics_offspring()
            print "Evaluating fitnesses" 
            self.evaluate_fitness_offspring()
            print "Selecting survivors"
            self.select_survivors()
            self.print_fitnesses(self.population)
            if self.boolStore == 1:   
                print "Inserting into database"
                self.InsertIndividuals(1)
                if 'novelty' in self.selectedFitnesses and self.fitnessParams[0]>0:
                    self.InsertIndividuals(2)
        
        #Return fittest front (excluding novelty)
        popPlusArchive = self.population + self.archive
        fits = list()
        for ind in popPlusArchive:
            fits.append([ind.potentialFitnesses[f] for f in self.selectedFitnesses if f != 'novelty'])
        fittestFrontIndIdsRel = non_dominated_sort(fits)[0]
        return [popPlusArchive[i] for i in fittestFrontIndIdsRel]


    def runRandom(self,numInd):
        self.results = list()

        counter = 0
        while len(self.results) < numInd:
            self.initialize_population()
            self.generate_semantics_offspring()
            self.evaluate_fitness_offspring()
            nonMurdered = [ind for ind in self.offspring if ind.boolMurdered == 0]
            self.results += copy.deepcopy(nonMurdered)
            self.print_fitnesses(self.results)
            counter+=1
        print "Murder counter: " + str(counter*numInd)
        return self.results[0:numInd]



    ##Debug is completely broken
    def debug(self,testEnd):
        if (testEnd>3): #we do parent selection, with NSGA=ii we need to have at least 2 individuals
            self.num_ind = 2
            self.num_tournament_participants = 2
        else:
            self.num_ind = 1 #2 if we want crossover
            self.num_tournament_participants = 1 #2 if we want crossover
        self.num_gen = 1

        self.mutation_prob = 1
        self.parent_selection_method = 1
        self.survivor_selection_method = 0
      
        self.build_front_cumulative_distributions()

        self.initialize_population()
        if (testEnd>0):
            self.generate_semantics_offspring()
        if (testEnd>1):
            self.evaluate_fitness_offspring()
        if (testEnd>2):
            self.select_survivors()
            self.print_fitnesses()
        if (testEnd>3):
            self.select_parents()
        if (testEnd>4):
            self.produce_offspring()
 


