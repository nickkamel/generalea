#ported from https://www.mathworks.com/matlabcentral/fileexchange/24390-mackey-glass-time-series-generator?focused=5119961&tab=example
import math
import numpy as np

def mackeyglass_eq(x_t, x_t_minus_tau, a, b):
    x_dot = -b * x_t + a * x_t_minus_tau / (1 + x_t_minus_tau ** 10.0)
    return x_dot

def mackeyglass_rk4(x_t, x_t_minus_tau, deltat, a, b):
    k1 = deltat*mackeyglass_eq(x_t,          x_t_minus_tau, a, b);
    k2 = deltat*mackeyglass_eq(x_t+0.5*k1,   x_t_minus_tau, a, b);
    k3 = deltat*mackeyglass_eq(x_t+0.5*k2,   x_t_minus_tau, a, b);
    k4 = deltat*mackeyglass_eq(x_t+k3,       x_t_minus_tau, a, b);
    x_t_plus_deltat = (x_t + k1/6 + k2/3 + k3/3 + k4/6);

    return x_t_plus_deltat

def generate_mk():
    a        = 0.2;    
    b        = 0.1;   
    tau      = 17;      
    x0       = 1.2;       
    deltat   = 1;  #was 0.1   
    sample_n = 1000;    
         
    history_length = int(math.floor(tau/deltat));
    history = [0]*history_length
    x_t = x0
    history.append(x0)
    
    for i in range (sample_n):
        x_t_minus_tau = history[i]
    
        x_t_plus_deltat = mackeyglass_rk4(x_t, x_t_minus_tau, deltat, a, b);
        #print x_t_plus_deltat
        history.append(x_t_plus_deltat)
        x_t = x_t_plus_deltat
    
    history = history[history_length:]
    #print history
    history_min = min(history)
    history_max = max(history)
    history_range = history_max-history_min
    
    history_scaled = list()
    for value in history:
        scaled_value = (value-history_min)/history_range
        history_scaled.append(scaled_value)
    
    return history_scaled
    #return history

def readin_sunspot():
    sunspot_data = np.genfromtxt ('sunspot.csv', delimiter=",")
    return sunspot_data

def generate_takens_training_data(dataset,tau,dimension):
	X_train = list() 
	y_train = list()
	X_test = list()
	y_test = list()

	dataset_length = len(dataset)
	time_series_train = dataset[0:dataset_length/2]
	time_series_test = dataset[dataset_length/2:dataset_length]
	time_series = [time_series_train,time_series_test]
	
	for id,time_series_subset in enumerate(time_series):       
		for window_start_index in range(len(time_series_subset)-dimension*tau):
			embedding = [time_series_subset[window_start_index+n*tau] for n in range(dimension)]
			correct_output = time_series_subset[window_start_index+(dimension-1)*tau+1] 
			if (id == 0):
				X_train.append(embedding)
				y_train.append(correct_output)
			else:
				X_test.append(embedding)
				y_test.append(correct_output)  
				
	return [X_train,y_train,X_test,y_test]